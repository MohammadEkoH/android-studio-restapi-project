<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller extends CI_Controller {
 
	public function index()
	{
        $this->load->view('vData');
    }
    
    function registrasi()
    { 
        $email = $this->input->post('email');
        $username = $this->input->post('username'); 
        $password = $this->input->post('password');

        $data = array(
            'email'     => $email,
            'username'  => $username, 
            'password'  => $password
        );

        $this->Mdbase->insert('user', $data);
    }


    function get_data_peserta()
    {
        $query = $this->Mdbase->getData('peserta'); 
        foreach ($query->result() as $row) {
            $data[] = array(
                'id'        => $row->id,
                'nama'      => $row->nama,
                'alamat'    => $row->alamat,
                'telepon'   => $row->telepon
            );
        }

        echo json_encode($data); 

        // $this->load->view('vData', $query);

    }

    function get_where_data_peserta($id)
    {
        $id = array('id' => $id);
        $query = $this->Mdbase->getWhereData('peserta', $id); 
        foreach ($query->result() as $row) {
            $data[] = array(
                'id'        => $row->id,
                'nama'      => $row->nama,
                'alamat'    => $row->alamat,
                'telepon'   => $row->telepon
            );
        }

        echo json_encode($data); 

        // $this->load->view('vData', $query);

    }

    function insert_peserta()
    {  
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $telepon = $this->input->post('telepon'); 

        $data = array( 
            'nama' => $nama,
            'alamat' => $alamat,
            'telepon' => $telepon 
        );

        $this->Mdbase->insert('peserta', $data);
        redirect('controller');
    }
 
    function update_peserta($id)
    {  
        $id = array('id' => $id);
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $telepon = $this->input->post('telepon'); 

        $data = array( 
            'nama' => $nama,
            'alamat' => $alamat,
            'telepon' => $telepon 
        );

        $this->Mdbase->update('peserta', $id, $data);
        redirect('controller');
    }

    function del_peserta($id)
    {
        $id = array('id' => $id); 

        $this->Mdbase->delete('peserta', $id);
        redirect('controller');
    }
}
