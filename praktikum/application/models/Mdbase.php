<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdbase extends CI_Model {
    function insert($table, $data)
    {
        $this->db->insert($table, $data);
    }

    function update($table, $id, $data)
    {
        $this->db->where($id);
        $this->db->udpate($table, $data);
    }

    function delete($table, $id)
    {
        $this->db->where($id);
        return $this->db->delete($table);
    }

    function getData($table)
    {
        return $this->db->get($table);
    } 

    function getWhereData($table, $data)
    {
        return $this->db->get_where($table, $data);
    } 
}
