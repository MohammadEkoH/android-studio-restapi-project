package praktikum.com.praktikum;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private EditText getIdPeserta, etNama, etAlamat, etTelepon;

    private MyAdapter adapter;
    private JsonArrayRequest arrayRequest;
    private RequestQueue requestQueue;
    private List<Peserta> lstData;
    private MyAdapter recyclerViewAdapter;

    private RecyclerView.LayoutManager layoutManager;

    private void init(){
        mRecyclerView   = findViewById(R.id.mRecyclerView);

        getIdPeserta    = findViewById(R.id.get_id_peserta);
        etNama          = findViewById(R.id.postNama);
        etAlamat        = findViewById(R.id.postAlamat);
        etTelepon       = findViewById(R.id.postTelepon);

        lstData = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setTitle("Home");
        setSupportActionBar(toolbar);

        init();

        showData();

        insertData();

    }


    private void insertData() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.custom_dialog_insert);
                dialog.setTitle("Add Data");
                dialog.show();

                Button btnInsertPeserta = dialog.findViewById(R.id.btn_insert_peserta);
                btnInsertPeserta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        inputData();
                    }
                });
            }
        });
    }

    private void inputData(){
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.POST, URL.INSERT_DATA_PESERTA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                System.out.println(response);
                Toast.makeText(getApplicationContext(),"Berhasil menambahkan data",Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Gagal menambahkan data",Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parameters  = new HashMap<String, String>();
                parameters.put("nama", etNama.getText().toString());
                parameters.put("alamat", etAlamat.getText().toString());
                parameters.put("telepon", etTelepon.getText().toString());

                return parameters;
            }
        };
        requestQueue.add(request);
    }

    private void showData() {
        String url = URL.GET_DATA_PESERTA;

        arrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                JSONObject jsonObject = null;

                for (int i = 0; i < response.length(); i++) {
                    try {
                        jsonObject = response.getJSONObject(i);
                        Peserta model = new Peserta();

                        model.setId(jsonObject.getString("id"));
                        model.setNama(jsonObject.getString("nama"));
                        model.setAlamat(jsonObject.getString("alamat"));
                        model.setTelepon(jsonObject.getString("telepon"));

                        lstData.add(model);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                setRvadapter(lstData);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error", error.toString());
            }
        });
        requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(arrayRequest);
    }

    public void setRvadapter (List<Peserta> lst){
        recyclerViewAdapter = new MyAdapter(MainActivity.this, lst);
        layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(recyclerViewAdapter);
    }
}
