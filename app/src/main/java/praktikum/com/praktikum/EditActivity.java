package praktikum.com.praktikum;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class EditActivity extends AppCompatActivity {

    public static String ID = "id", NAMA = "nama", ALAMAT = "alamat", TELEPON = "telepon";

    private EditText etNama, etAlamat, etTelepon;

    private void init(){
        etNama      = findViewById(R.id.post_nama);
        etAlamat    = findViewById(R.id.post_alamat);
        etTelepon   = findViewById(R.id.post_telepon);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        init();

        String id = getIntent().getExtras().getString(ID);
        String nama = getIntent().getExtras().getString(NAMA);
        String alamat = getIntent().getExtras().getString(ALAMAT);
        String telepon = getIntent().getExtras().getString(TELEPON);
//
//        etNama.setText(nama);
//        etAlamat.setText(alamat);
//        etTelepon.setText(telepon);
    }


}
