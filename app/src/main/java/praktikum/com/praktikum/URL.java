package praktikum.com.praktikum;

public class URL {
    public static final String
            BASE_URL                = "http://172.17.100.2/praktikum/",
            INSERT_DATA_REGIS       = BASE_URL+"controller/registrasi/",
            GET_DATA_PESERTA        = BASE_URL+"controller/get_data_peserta/",
            GET_WHERE_DATA_PESERTA  = BASE_URL+"controller/get_where_data_peserta/",
            INSERT_DATA_PESERTA     = BASE_URL+"controller/insert_peserta/",
            UPDATE_DATA_PESERTA     = BASE_URL+"controller/update_peserta/",
            DELETE_DATA_PESERTA     = BASE_URL+"controller/del_peserta/";
}
