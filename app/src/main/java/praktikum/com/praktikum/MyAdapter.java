package praktikum.com.praktikum;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyAdapter  extends RecyclerView.Adapter<MyAdapter.MahasiswaViewHolder> {
    private List<Peserta> dataList;
    private Context context;
    private RequestQueue requestQueue;


    public MyAdapter(Context context, List dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    public class MahasiswaViewHolder extends RecyclerView.ViewHolder {

        private EditText getIdPeserta;
        private TextView txtNama, txtAlamat, txtTelepon;
        private LinearLayout btn_dialog_select;
        private Button btnEdit, btnDel;

        public MahasiswaViewHolder(final View itemView) {
            super(itemView);
            getIdPeserta    = itemView.findViewById(R.id.get_id_peserta);

            txtNama         = itemView.findViewById(R.id.tv_nama);
            txtAlamat       = itemView.findViewById(R.id.tv_alamat);
            txtTelepon      = itemView.findViewById(R.id.tv_telepon);

            btn_dialog_select = itemView.findViewById(R.id.btn_show_dialog);

            btn_dialog_select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.custom_dialog_select);
                    dialog.setTitle("Select");
                    dialog.show();

                    btnEdit = dialog.findViewById(R.id.btn_edit);
                    btnDel = dialog.findViewById(R.id.btn_delete);

                    btnEdit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(context, "edit", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, EditActivity.class);
                            //edit activity bosquee duduk edittext.class
                            intent.putExtra("id", getIdPeserta.getText()+"");
                            intent.putExtra("nama", txtNama.getText()+"");
                            intent.putExtra("alamat", txtAlamat.getText()+"");
                            intent.putExtra("telepon", txtTelepon.getText()+"");

                            context.startActivity(intent);
                        }
                    });

                    btnDel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deletePeserta(getIdPeserta.getText().toString());
                            dialog.dismiss();
                        }
                    });
                }
            });

        }
    }

    @Override
    public MahasiswaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_peserta, parent, false);
        return new MahasiswaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MahasiswaViewHolder holder, int position) {
        holder.getIdPeserta.setText(dataList.get(position).getId());
        holder.txtNama.setText(dataList.get(position).getNama());
        holder.txtAlamat.setText(dataList.get(position).getAlamat());
        holder.txtTelepon.setText(dataList.get(position).getTelepon());

    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;

    }

    private void deletePeserta(String id) {
        requestQueue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(Request.Method.POST, URL.DELETE_DATA_PESERTA+"/"+id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response.toString());

                Toast.makeText(context,"Berhasil dihapus",Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"Gagal dihapus",Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> parameters  = new HashMap<String, String>();
                return parameters;
            }
        };
        requestQueue.add(request);
    }

}